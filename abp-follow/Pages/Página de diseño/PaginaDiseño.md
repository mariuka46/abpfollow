# Página de diseño

## Wireframe y Mockup

A lo largo del proceso de diseño, se ha tenido que tomar varias decisiones importantes. 
Una de las más significativas fue la decisión de alejarnos del diseño inicial de los wireframes. 
Aunque el diseño inicial era funcional, le faltaba la chispa que hace que una aplicación sea 
realmente atractiva para los usuarios. Al final, sé optó por un diseño más vibrante y atractivo.

Diseño original del wireframe:

![Login.png](Login.png)

Diseño final del mockup:

![login2.jpg](login2.jpg)

## Comparativa y toma de decisiones

Dado que el diseño original no ha gustado lo suficiente se ha optado por modificar todo el diseño de todas las pantallas, dado que parecía muy aburrido y poco original.

Se ha mantenido un poco los colores iniciales como el azul para el fondo aunque se ha añadido un degradado en colores crema porque así lo hace más atractivo y la verdad que el diseño final ha quedado mucho mejor y atractiva.

**Pantallas**

Artefactos diseño inicial

![Artefactos.png](Artefactos.png)

Imagen pantalla de artefacto diseño inicial

![artefacto2.jpg](artefacto2.jpg)
![artefacto3.jpg](artefacto3.jpg)

Imagen pantalla de artefacto diseño final

Como se puede observar los diseños finales son mucho mejores, se ha integrado un carroussel para que se vean más divertido la recopilación de los artefactos, también se ha dejado como muestra el artefacto que estás buscando, pero si no lo has coseguido se muestra una imagen de NOT FOUND y tampoco se puede acceder a la información del artefacto hasta que no lo hayas obtenido, las imagenes que hemos integrado para cuando el usuario recoja los artefactos también son más animadas y de mejor calidad.
El diseño final se adapta mucho mejor a lo que esperábamos de esta aplicación

**Información del artefacto una vez escaneado**

![infoartefacto.png](infoartefacto.png)

Imagen información artefacto primer diseño

![infoartefacto2.jpg](infoartefacto2.jpg)

Imagen información artefacto diseño final

El diseño de la navegación del menu lo hemos mantenido como en un principio.

