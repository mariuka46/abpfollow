# G3


# Proyecto Follow


![logo.png](..%2F..%2FReactNative%2FAplicativo%2Fsrc%2FFiles%2Flogo.png)

---
**Título:** "Follow"

**Autor:**
- Maria Cosa

**Fecha:** "10/01/2024"

---

## Presentación de los componentes del grupo
Entusiasta aprendiz de informática, se compromete a llevar a cabo adelante el proyecto así como dedicar tiempo a poder aprender lo que le hace falta para sacar adelante todos los contratiempos que se puedan presentar a lo largo del desarrollo, sacrificando noches de sueño para sacar adelante la primera entrega del proyecto

## Presentación del proyecto

Este proyecto es una aplicación de geolocalización de artefactos con un enfoque social, permitiendo a los usuarios explorar la ciudad y así descubrir nuevos rincones, también queríamos darle un enfoque más social, pero por la distribución del tiempo ha sido imposible gestionarlo en esta entrega y está pendiente de entregar en la siguiente actualización... 

Tratamos tanto el desarrollo de backend como de frontend.

El objetivo principal de la aplicación es proporcionar una experiencia de juego divertida y atractiva para los usuarios. Los usuarios pueden explorar su entorno en busca de artefactos, añadiendo un elemento de aventura y descubrimiento al juego. Al mismo tiempo, pueden compartir sus hallazgos con otros usuarios, fomentando la interacción y la comunidad.

Comprometida con la creación de una plataforma que sea fácil de usar tanto para los usuarios como para los administradores.

El enfoque está en la simplicidad y la facilidad de uso, asegurándo de que todos puedan disfrutar de la aplicación sin problemas.

En resumen, este proyecto es una combinación de juego, exploración, interacción social y tecnología, todo en una plataforma fácil de usar.

¡Esperamos que los usuarios pasen un buen rato utilizando nuestra aplicación! 😊