## Organización del proyecto 

![organizacion.jpg](organizacion.jpg)


### Gitlab y manejo de issues 

En este proyecto, se han realizado, creación de ramas, fusión de cambios y la resolución de conflictos que pueden surgir durante la integración de las ramas en la rama principal.

A lo largo del desarrollo del proyecto, se han realizado un total de 117 commits y trabajado con 17 ramas diferentes.

### Milestones y reparto de tareas

![img_1.png](img_1.png)

![img.png](img.png)

Para la realización de este proyecto, se han establecido 4 milestones, cada uno correspondiente a un sprint específico. Cada milestone representa una etapa importante en el desarrollo del proyecto y ayuda a mantenerse enfocado y organizado.

El **primer milestone**, estableció la base de la aplicación.

El objetivo principal de este primer sprint fue dar los primeros pasos en la creación de la aplicación y definir su estructura y funcionalidad básica.

El **segundo milestone** estuvo dedicado a la implementación del back-end de la aplicación. Durante este sprint, se estableció la estructura básica del back-end, aunque después se tuvo que adaptar a lo largo del proyecto para satisfacer las necesidades cambiantes.

El **tercer milestone** primer contacto con React Native, el framework elegido para el desarrollo del front-end. Este sprint se dedicó principalmente a diseñar las páginas esenciales de la aplicación.

Finalmente, el **cuarto milestone** estuvo dedicado a los toques finales y a la corrección de funcionalidades del proyecto. Este sprint permitió pulir la aplicación y asegurarse de que todo funcionara como se esperaba.


### Sprint Reviews y Sprint Plannings
![img_2.png](img_2.png)

Cada semana, se realizába una reunión de **Sprint Planning**.

Además, teníamos un tiempo establecido para realizar **las Sprints Reviews**. Estas reuniones se llevaban a cabo cada semana, o como máximo cada semana y media. 
Durante las Sprint Reviews, se revisaba y evaluaba lo que se había realizado durante el sprint.

El objetivo siempre ha sido llegar a los objetivos marcados, y se ha intentado hacer todo lo posible para lograrlo.